using System;
using System.Collections;
using UnityEngine;

namespace Runtime.Utilities {
  public class InvokeUtils {
    public static void Invoke(MonoBehaviour mb, Action f, float delay)
    {
      mb.StartCoroutine(InvokeRoutine(f, delay));
    }
 
    private static IEnumerator InvokeRoutine(Action f, float delay)
    {
      yield return new WaitForSeconds(delay);
      f();
    }
  }
}