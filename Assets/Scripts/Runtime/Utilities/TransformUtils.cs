using UnityEngine;

namespace Runtime.Utilities {
  public class TransformUtils {
    public static void SetLayerRecursively(GameObject gameObject, int layerNumber)
    {
      foreach (Transform trans in gameObject.GetComponentsInChildren<Transform>(true))
      {
        trans.gameObject.layer = layerNumber;
      }
    }
  }
}