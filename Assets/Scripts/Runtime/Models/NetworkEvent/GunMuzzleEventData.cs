using System;
using UnityEngine;

namespace Runtime.Models.Event {
  [Serializable]
  public class GunMuzzleEventData : BasePhotonEventData {
    [SerializeField]
    private Vector3 firePoint;

    [SerializeField]
    private string ammoAssetName;

    public GunMuzzleEventData(int viewID, Vector3 firePoint, string ammoAssetName) : base(viewID) {
      this.firePoint = firePoint;
      this.ammoAssetName = ammoAssetName;
    }

    public string AmmoAssetName {
      get {
        return ammoAssetName;
      }
    }

    public Vector3 FirePoint {
      get {
        return firePoint;
      }
    }
  }
}