using System;
using UnityEngine;

namespace Runtime.Models.Event {
  [Serializable]
  public class BasePhotonEventData {
    [SerializeField]
    protected int viewID;

    protected BasePhotonEventData(int viewID) {
      this.viewID = viewID;
    }

    public int ViewID {
      get {
        return viewID;
      }
    }
  }
}