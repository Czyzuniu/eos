using System;
using UnityEngine;

namespace Runtime.Models.Event {
  [Serializable]
  public class WeaponShootProjectileEventData : BasePhotonEventData {
    [SerializeField]
    private Vector3 spawnPoint;
    [SerializeField]
    private Vector3 point;
    [SerializeField]
    private string ammoAssetName;
    [SerializeField]
    private Vector3 direction;

    public Vector3 Direction {
      get {
        return direction;
      }
    }
    public WeaponShootProjectileEventData(int viewID, Vector3 spawnPoint, Vector3 point, string ammoAssetName, Vector3 direction) : base(viewID) {
      this.spawnPoint = spawnPoint;
      this.point = point;
      this.ammoAssetName = ammoAssetName;
      this.direction = direction;
    }

    public string AmmoAssetName {
      get {
        return ammoAssetName;
      }
    }

    public Vector3 SpawnPoint {
      get {
        return spawnPoint;
      }
    }

    public Vector3 Point {
      get {
        return point;
      }
    }
  }
}