using System;
using UnityEngine;

namespace Runtime.Models.Event {
  [Serializable]
  public class HitInfoEventData : BasePhotonEventData {
    [SerializeField]
    private float damage;

    public HitInfoEventData(int viewID, float damage) : base(viewID) {
      this.damage = damage;
    }

    public float Damage {
      get {
        return damage;
      }
    }
  }
}