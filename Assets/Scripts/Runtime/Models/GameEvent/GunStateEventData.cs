namespace Runtime.Models.GameEvent {
  public class GunStateEventData {
    private readonly int currentAmmo;
    private readonly int totalAmmo;

    public GunStateEventData(int currentAmmo, int totalAmmo) {
      this.currentAmmo = currentAmmo;
      this.totalAmmo = totalAmmo;
    }

    public int CurrentAmmo {
      get {
        return currentAmmo;
      }
    }
    public int TotalAmmo {
      get {
        return totalAmmo;
      }
    }
  }
}