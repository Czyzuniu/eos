using System;
using UnityEngine;

namespace Runtime.Models {
  [Serializable]
  public class HitData {
    [SerializeField]
    private float damage;

    [SerializeField]
    private Transform receiver;

    public HitData(float damage, Transform receiver) {
      this.damage = damage;
      this.receiver = receiver;
    }


    public float Damage {
      get {
        return damage;
      }
    }
    public Transform Receiver {
      get {
        return receiver;
      }
    }
  }
}