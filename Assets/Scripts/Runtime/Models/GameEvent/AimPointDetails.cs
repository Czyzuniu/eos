using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Runtime.Models {
  public class AimPointDetails {
    private readonly Vector3 aimPoint;
    private readonly Vector3 normal;
    private readonly GameObject target;

    public AimPointDetails(Vector3 aimPoint, Vector3 normal, GameObject target) {
      this.aimPoint = aimPoint;
      this.normal = normal;
      this.target = target;
    }
    public Vector3 AimPoint {
      get {
        return aimPoint;
      }
    }
    public GameObject Target {
      get {
        return target;
      }
    }
    public Vector3 Normal {
      get {
        return normal;
      }
    }
  }
}