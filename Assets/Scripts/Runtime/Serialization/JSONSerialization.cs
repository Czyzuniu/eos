using UnityEngine;

namespace Runtime.Serialization {
  public class JSONSerialization {
    public static string Serialize<T>(T data) {
      return JsonUtility.ToJson(data);
    }
    
    public static T Deserialize<T>(string data) {
      return JsonUtility.FromJson<T>(data);
    }
  }
}