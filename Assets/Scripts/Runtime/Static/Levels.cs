using UnityEngine;

namespace Runtime.Static
{
    public class Levels {
        public static readonly string SandBox = "Sandbox";
        public static readonly string DeathMatch = "DeathMatch";
        public static readonly string Kulfonlandia = "Kulfonlandia";
    }
}