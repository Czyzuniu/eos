using UnityEngine;

namespace Runtime.Static
{
    public class AnimatorConstants
    {
        public static readonly int Forward = Animator.StringToHash("Forward");
        public static readonly int Directional = Animator.StringToHash("Direction");
        public static readonly int ReloadTrigger = Animator.StringToHash("Reload");
    }
}