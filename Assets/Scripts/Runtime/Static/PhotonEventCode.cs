namespace Runtime.Static
{
    public class PhotonEventCode
    {
        public static readonly byte SpawnWeaponMuzzleFlashEventCode = 1;
        public static readonly byte ShootProjectileEventCode = 2;
        public static readonly byte HitReceivedEventCode = 3;
    }
}