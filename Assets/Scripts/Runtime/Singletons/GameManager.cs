using System;
using System.IO;
using Photon.Pun;
using Runtime.Behaviours.Controller.Aim;
using Runtime.Behaviours.Controller.Movement;
using Runtime.Static;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour {
  [SerializeField]
  private GameObjectGameEvent playerInitialisedGameEvent;
    
  private static GameManager _instance;
  public static GameManager Instance { get { return _instance; } }

  [SerializeField]
  private GameObject _controlledPlayer;

  private void Awake() {
    if (_instance != null && _instance != this) {
      Destroy(gameObject);
    }
    else {
      _instance = this;
      DontDestroyOnLoad(gameObject);
    }
  }

  private void Start() {
    ReSpawnPlayer();
  }

  private Vector3 FindSpawnPoint() {
    return Vector3.zero;
  }

  public void ReSpawnPlayer() {
    if (_controlledPlayer) {
      PhotonNetwork.Destroy(_controlledPlayer);
    }
    GameObject player = PhotonNetwork.Instantiate(Path.Combine(PathConstants.PREFABS_PATH, "Characters/SoldierFP"), FindSpawnPoint(), Quaternion.identity);
    playerInitialisedGameEvent.Raise(player);
    _controlledPlayer = player;
  }
}