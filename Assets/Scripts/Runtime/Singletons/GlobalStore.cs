using CODE_GENERATION.Events.Game_Events;
using RotaryHeart.Lib.SerializableDictionary;
using Runtime.Models;
using Runtime.ScriptableObjects;
using Runtime.ScriptableObjects.Entity;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Runtime.Singletons {
  public class GlobalStore : MonoBehaviour {
    private static GlobalStore _instance;

    [SerializeField]
    private AimPointDetailsGameEvent _aimPointDetailsGameEvent;

    public static GlobalStore Instance { get { return _instance; } }

    [SerializeField]
    private SerializableDictionaryBase<string, Entity> assetDatabase;

    public AimPointDetails PointDetails { get; set; }
    public SerializableDictionaryBase<string, Entity> AssetDatabase {
      get {
        return assetDatabase;
      }
    }


    private void Awake() {
      if (_instance != null && _instance != this) {
        Destroy(gameObject);
      }
      else {
        _instance = this;
        DontDestroyOnLoad(gameObject);
        _aimPointDetailsGameEvent.AddListener(((aim) => PointDetails = aim));

        var assets = Resources.LoadAll("Scriptables", typeof(Entity));
        foreach (Entity t in assets) {
          AssetDatabase.Add(t.name, t);
        }
      }
    }
  }
}