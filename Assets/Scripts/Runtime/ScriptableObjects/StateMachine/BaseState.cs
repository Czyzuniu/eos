using UnityEngine;

namespace Runtime.ScriptableObjects.StateMachine {
  public abstract class BaseState : ScriptableObject {
    public virtual void OnEnter(GameObject gameObject) { }
    public virtual void OnUpdate(GameObject gameObject) { }
    public virtual void OnExit(GameObject gameObject) { }
  }
}