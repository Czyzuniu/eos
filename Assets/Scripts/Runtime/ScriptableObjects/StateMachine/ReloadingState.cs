using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace Runtime.ScriptableObjects.StateMachine {
  [CreateAssetMenu(fileName = "StateMachine", menuName = "States/Reload", order = 0)]
  public class ReloadingState : BaseState {
    private RigBuilder _rigBuilder;
    public override void OnEnter(GameObject gameObject) {
      _rigBuilder = gameObject.GetComponent<RigBuilder>();
      foreach (RigLayer rigBuilderLayer in _rigBuilder.layers) {
        rigBuilderLayer.active = false;
      }
    }
    
    public override void OnExit(GameObject gameObject) {
      foreach (RigLayer rigBuilderLayer in _rigBuilder.layers) {
        rigBuilderLayer.active = true;
      }
    }
  }
}