using Runtime.Behaviours.Controller.Aim;
using Runtime.Behaviours.Controller.Movement;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.ScriptableObjects.StateMachine {
  [CreateAssetMenu(fileName = "StateMachine", menuName = "States/Death", order = 2)]
  public class DeathState : BaseState {
    public override void OnEnter(GameObject gameObject) {
      gameObject.GetComponent<MovementController>().enabled = false;
      gameObject.GetComponent<AimController>().enabled = false;
      gameObject.GetComponent<PlayerInput>().DeactivateInput();
      gameObject.GetComponent<Rigidbody>().useGravity = false;
      gameObject.GetComponent<Collider>().enabled = false;
    }
  }
}