using Runtime.Static;
using UnityEngine;

namespace Runtime.ScriptableObjects {
  [CreateAssetMenu(fileName = "Command", menuName = "Commands/ReloadCommand", order = 1)]
  public class ReloadCommand : Command {
    private Animator _animator;

    public override void ExecuteAction(GameObject gameObject) {
      if (!_animator) {
        _animator = gameObject.GetComponent<Animator>();
      }
      _animator.SetTrigger(AnimatorConstants.ReloadTrigger);
    }
  }
}