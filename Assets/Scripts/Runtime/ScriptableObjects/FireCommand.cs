using Runtime.Behaviours.Controller.Character.Base;
using Runtime.Interfaces;
using UnityEngine;

namespace Runtime.ScriptableObjects {
  [CreateAssetMenu(fileName = "Command", menuName = "Commands/FireCommand", order = 0)]
  public class FireCommand : Command {
    private BaseCharacter _baseCharacter;

    public override void ExecuteAction(GameObject gameObject) {
      if (_baseCharacter == null) {
        _baseCharacter = gameObject.GetComponent<BaseCharacter>();
      }
      if (_baseCharacter is IAttack iAttack) {
        iAttack.Fire();
      }
    }
  }
}