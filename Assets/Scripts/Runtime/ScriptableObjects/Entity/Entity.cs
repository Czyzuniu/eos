using UnityEngine;

namespace Runtime.ScriptableObjects.Entity {
  [CreateAssetMenu(fileName = "Entity", menuName = "Game Entity", order = 0)]
  public abstract class Entity : ScriptableObject {
    
  }
}