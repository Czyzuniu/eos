using UnityEngine;

namespace Runtime.ScriptableObjects.Entity {
  [CreateAssetMenu(fileName = "Entity", menuName = "Entity/Gun", order = 0)]
  public class Gun : Entity {
    [SerializeField]
    private float rateOfFire;
    [SerializeField]
    private int clipSize;
    [SerializeField]
    private Ammo ammo;

    [SerializeField]
    private AudioClip firingSoundEffect;

    public AudioClip FiringSoundEffect {
      get {
        return firingSoundEffect;
      }
    }

    public Ammo Ammo {
      get {
        return ammo;
      }
    }

    public float RateOfFire {
      get {
        return rateOfFire;
      }
    }
    public int ClipSize {
      get {
        return clipSize;
      }
    }
  }
}