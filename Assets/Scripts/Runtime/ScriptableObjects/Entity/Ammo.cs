using UnityEngine;

namespace Runtime.ScriptableObjects.Entity {
  [CreateAssetMenu(fileName = "Entity", menuName = "Entity/Ammo", order = 1)]
  public class Ammo : Entity {
    [SerializeField]
    private GameObject muzzleFx;
    [SerializeField]
    private GameObject projectileFx;
    [SerializeField]
    private GameObject hitFx;
    [SerializeField]
    private AudioSource muzzleAudio;


    public GameObject MuzzleFx {
      get {
        return muzzleFx;
      }
    }
    public GameObject ProjectileFx {
      get {
        return projectileFx;
      }
    }
    public GameObject HitFx {
      get {
        return hitFx;
      }
    }
    public AudioSource MuzzleAudio {
      get {
        return muzzleAudio;
      }
    }
  }
}