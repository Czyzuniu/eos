using System.Collections.Generic;
using Runtime.ScriptableObjects.StateMachine;
using UnityEngine;

namespace Runtime.ScriptableObjects {
  public abstract class Command : ScriptableObject {
    [SerializeField]
    private List<BaseState> _allowedStates;
    private StateMachineController _stateMachineController;
    public void Execute(GameObject gameObject) {
      if (CanExecute(gameObject)) {
        ExecuteAction(gameObject);
      }
    }

    public abstract void ExecuteAction(GameObject gameObject);

    private bool CanExecute(GameObject gameObject) {
      if (!_stateMachineController) {
        _stateMachineController = gameObject.GetComponent<StateMachineController>();
      }
      if (_allowedStates.Count == 0) {
        return true;
      }
      
      return _allowedStates.Find(b => b.name.Equals(_stateMachineController.CurrentState.name));
    }
  }
}