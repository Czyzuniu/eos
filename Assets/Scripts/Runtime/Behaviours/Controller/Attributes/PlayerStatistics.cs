using Photon.Pun;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Attributes {
  public class PlayerStatistics : MonoBehaviourPun, IPunObservable {
    [SerializeField]
    private FloatGameEvent _healthChangedEvent;
    [SerializeField]
    private GameEvent _playerDeathGameEvent;
    [SerializeField]
    private float health;
    private Animator _animator;
    private bool isDead;

    public bool IsDead {
      get {
        return isDead;
      }
    }

    private void Awake() {
      _animator = GetComponent<Animator>();
    }
    private void Start() {
      SetHealth(250);
    }

    public void DecreaseHealth(float toDecrease) {
      SetHealth(health - toDecrease);

    }

    private void SetHealth(float newHealth) {
      health = newHealth;
      isDead = health <= 0;
      if (isDead) {
        _animator.SetTrigger("Death");
      }
      if (photonView.IsMine) {
        _healthChangedEvent.Raise(health);
        if (isDead) {
          _playerDeathGameEvent.Raise();
        }
      }
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
      if (stream.IsWriting) {
        stream.SendNext(health);
      } else {
        health = (float)stream.ReceiveNext();
      }
    }

  }
}