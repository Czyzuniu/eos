using Runtime.Behaviours.Controller.Input;
using UnityEngine;

namespace Runtime.Behaviours.Controller.UI {
    public class DeathPanelController : MonoBehaviour {
        [SerializeField]
        private GameObject deathScreen;
        public void TogglePanel(bool toggle) {
            deathScreen.SetActive(toggle);
        }
    }
}
