using Photon.Pun;

public class RespawnButton : MonoBehaviourPun
{
    public void OnRespawnPressed() {
        if (photonView.IsMine) {
            GameManager.Instance.ReSpawnPlayer();
        }
    }
}
