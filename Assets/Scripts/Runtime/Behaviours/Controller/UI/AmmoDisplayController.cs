using System;
using Michsky.UI.Shift;
using Runtime.Models.GameEvent;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Runtime.Behaviours.Controller.UI {
  public class AmmoDisplayController : MonoBehaviour {
    [SerializeField]
    private GunStateEventDataGameEvent gunStateGameEventListener;
    private UIManagerText _uiManagerText;
    private string initialPlaceholderText;

    private void Awake() {
      _uiManagerText = GetComponent<UIManagerText>();
      initialPlaceholderText = _uiManagerText.textObject.text;
    }

    private void OnGameFiredEventListener(GunStateEventData eventData) {
      _uiManagerText.textObject.text = String.Format(initialPlaceholderText, new object[] {
        "Brutality-41", eventData.CurrentAmmo, eventData.TotalAmmo
      });
    }
    
    private void OnEnable() {
      gunStateGameEventListener.AddListener(OnGameFiredEventListener);
    }

    private void OnDisable() {
      gunStateGameEventListener.RemoveListener(OnGameFiredEventListener);
    }

  }
}