using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.UI;

public class HealthSliderController : MonoBehaviour {
    [SerializeField]
    private FloatGameEvent _healthEventListener;
    private Slider _slider;

    private void Awake() {
        _slider = GetComponent<Slider>();
    }
    private void UpdateHealth(float newHealth) {
        _slider.value = newHealth;
    }
    
    private void OnEnable() {
        _healthEventListener.AddListener(UpdateHealth);
    }
    
    private void OnDisable() {
        _healthEventListener.RemoveListener(UpdateHealth);
    }
}
