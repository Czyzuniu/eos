using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Runtime.Behaviours.Controller.Attributes;
using Runtime.Behaviours.Controller.Projectile;
using Runtime.Models.Event;
using Runtime.ScriptableObjects;
using Runtime.ScriptableObjects.Entity;
using Runtime.Serialization;
using Runtime.Singletons;
using Runtime.Static;
using Runtime.Utilities;
using UnityEngine;

namespace Runtime.Behaviours.Controller.EventListener {
  public class PlayerPhotonEventListener : MonoBehaviourPun, IOnEventCallback {
    private PlayerStatistics _statistics;
    private List<byte> reservedEvents = new List<byte> {
      0,
      253,
      201,
      206
    };
    private void Awake() {
      _statistics = GetComponent<PlayerStatistics>();
    }
    
    private void OnEnable() {
      PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable() {
      PhotonNetwork.RemoveCallbackTarget(this);
    }
    
    public void OnEvent(EventData photonEvent) {
      if (!reservedEvents.Contains(photonEvent.Code)) {
        BasePhotonEventData baseData = JSONSerialization.Deserialize<BasePhotonEventData>(photonEvent.CustomData as string);
        if (baseData != null ) {
          if (!baseData.ViewID.Equals(photonView.ViewID)) {
            return;
          }
          if (photonEvent.Code.Equals(PhotonEventCode.ShootProjectileEventCode)) {
            HandleShootProjectileEventCode(photonEvent);
          } else if (photonEvent.Code.Equals(PhotonEventCode.SpawnWeaponMuzzleFlashEventCode)) {
            HandleSpawnWeaponMuzzleFlashEventCode(photonEvent);
          } else if (photonEvent.Code.Equals(PhotonEventCode.HitReceivedEventCode)) {
            HandleHitReceivedEventCode(photonEvent);
          }
        }
      }
    }
    private void HandleHitReceivedEventCode(EventData photonEvent) {
      var data = JSONSerialization.Deserialize<HitInfoEventData>(photonEvent.CustomData as string);
      _statistics.DecreaseHealth(data.Damage);
    }

    private void HandleSpawnWeaponMuzzleFlashEventCode(EventData photonEvent) {
      var data = JSONSerialization.Deserialize<GunMuzzleEventData>(photonEvent.CustomData as string);
      GameObject prefab = (GlobalStore.Instance.AssetDatabase[data.AmmoAssetName] as Ammo)?.MuzzleFx;
      Destroy(Instantiate(prefab, data.FirePoint, Quaternion.identity, transform), 5f);
    }

    private void HandleShootProjectileEventCode(EventData photonEvent) {
      var data = JSONSerialization.Deserialize<WeaponShootProjectileEventData>(photonEvent.CustomData as string);
      Ammo ammo = GlobalStore.Instance.AssetDatabase[data.AmmoAssetName] as Ammo;
      GameObject prefab = ammo.ProjectileFx;
      GameObject projectile = Instantiate(prefab, data.SpawnPoint, Quaternion.identity);
      TransformUtils.SetLayerRecursively(projectile, LayerMask.NameToLayer(photonView.IsMine ? "Projectile" : "ProjectileOther"));
      projectile.AddComponent<ProjectileController>().Fire(data, ammo);
    }
  }
}