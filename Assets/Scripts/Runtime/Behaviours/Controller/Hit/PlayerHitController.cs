using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Runtime.Interfaces;
using Runtime.Models;
using Runtime.Models.Event;
using Runtime.Serialization;
using Runtime.Static;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Hit {
  public class PlayerHitController : MonoBehaviourPun, IDamageable {
    public void ITakeDamage(HitData hitData) {
      if (!photonView.IsMine) {
        return;
      }
      
      RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
        Receivers = ReceiverGroup.All
      };
      PhotonNetwork.RaiseEvent(PhotonEventCode.HitReceivedEventCode,
        JSONSerialization.Serialize(new HitInfoEventData(photonView.ViewID, hitData.Damage)),
        raiseEventOptions, SendOptions.SendReliable);
    }
  }
}