using Photon.Pun;
using Runtime.Behaviours.Controller.Player;
using Runtime.Models;
using Runtime.Singletons;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Rigging {
  public class AnimationRigController : MonoBehaviourPun {
    [SerializeField]
    private GameObject target;
    


    private void Awake() {
      enabled = photonView.IsMine;
    }

    public void OnPlayerInitialised(GameObject player) {
      target = player.GetComponent<PlayerConfig>().AnimationAimRigTarget;
    }

    private void SetTargetToAimTarget(AimPointDetails aimPointDetails) {
      Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
      Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
      if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, 1 << LayerMask.NameToLayer("Static"))) {
        
      }
      
      target.transform.position = Vector3.Lerp(target.transform.position, raycastHit.point, Time.deltaTime * 20f);
    }

    private void Update() {
      SetTargetToAimTarget(GlobalStore.Instance.PointDetails);
    }
  }
}