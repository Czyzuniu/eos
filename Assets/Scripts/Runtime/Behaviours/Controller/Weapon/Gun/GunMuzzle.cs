using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Runtime.Interfaces;
using Runtime.Models.Event;
using Runtime.Serialization;
using Runtime.Static;

namespace Runtime.Behaviours.Controller.Weapon.Gun {
  public class GunMuzzle : MonoBehaviourPun, IMuzzle {
    private Gun _gun;

    private void Awake() {
      _gun = GetComponentInParent<Gun>();
    }

    public void Flash() {
      RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
        Receivers = ReceiverGroup.All
      };
      PhotonNetwork.RaiseEvent(PhotonEventCode.SpawnWeaponMuzzleFlashEventCode,
        JSONSerialization.Serialize(new GunMuzzleEventData(photonView.ViewID, transform.position, _gun.ScriptableGun.Ammo.name)),
        raiseEventOptions, SendOptions.SendUnreliable);
    }
  }
}