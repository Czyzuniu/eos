using Photon.Pun;
using Runtime.Interfaces;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Weapon.Gun {
  public class Gun : MonoBehaviourPun, IGun {
    [SerializeField]
    private ScriptableObjects.Entity.Gun scriptableGun;
    private IMuzzle _muzzle;
    private IBarrel _barrel;
    private IGunState _gunState;

    public ScriptableObjects.Entity.Gun ScriptableGun {
      get {
        return scriptableGun;
      }
    }

    private void Awake() {
      _muzzle = GetComponentInChildren<IMuzzle>();
      _barrel = GetComponentInChildren<IBarrel>();
      _gunState = GetComponent<IGunState>();
    }

    public void Fire() {
      if (_gunState.CanFire()) {
        _muzzle?.Flash();
        _barrel?.Fire();
        _gunState.UseAmmo();
      }
    }
    public void Reload() {
      _gunState.Reload();
    }
  }
}