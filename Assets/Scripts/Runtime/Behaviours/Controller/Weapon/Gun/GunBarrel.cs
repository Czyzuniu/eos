using System;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Runtime.Interfaces;
using Runtime.Models.Event;
using Runtime.Serialization;
using Runtime.Singletons;
using Runtime.Static;
using Runtime.Utilities;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Weapon.Gun {
  public class GunBarrel : MonoBehaviourPun, IBarrel {
    private GunAudioController _audioController;
    private Gun _gun;

    private void Awake() {
      _gun = GetComponentInParent<Gun>();
    }

    private void Start() {
      _audioController = GetComponentInParent<GunAudioController>();
    }

    public void Fire() {
      RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
        Receivers = ReceiverGroup.All
      };
      
      string shootEventData = JSONSerialization.Serialize(
        new WeaponShootProjectileEventData(
          photonView.ViewID,
          transform.position,
          GlobalStore.Instance.PointDetails.AimPoint,
          _gun.ScriptableGun.Ammo.name,
          transform.forward
        )
      );

      PhotonNetwork.RaiseEvent(PhotonEventCode.ShootProjectileEventCode,
        shootEventData,
        raiseEventOptions, SendOptions.SendReliable);

      _audioController.PlayFireSound();
    }
  }
}
