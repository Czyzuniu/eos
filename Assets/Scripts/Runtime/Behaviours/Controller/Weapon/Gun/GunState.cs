using Runtime.Interfaces;
using Runtime.Models.GameEvent;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Weapon.Gun {
  public class GunState : MonoBehaviour, IGunState {
    private float lastShot;
    [SerializeField]
    protected GunStateEventDataGameEvent _gunStateChangedGameEvent;

    [SerializeField]
    private int currentAmmo;

    private Gun _gun;

    private void Awake() {
      _gun = GetComponent<Gun>();
      Reload();
    }
    
    public bool CanFire() {
      return Time.time > _gun.ScriptableGun.RateOfFire + lastShot && GetCurrentAmmo() > 0;
    }

    public int GetCurrentAmmo() {
      return currentAmmo;
    }
    
    public void UseAmmo() {
      currentAmmo -= 1;
      lastShot = Time.time;
      _gunStateChangedGameEvent?.Raise(new GunStateEventData(currentAmmo, _gun.ScriptableGun.ClipSize));
    }
    
    public void Reload() {
      currentAmmo = _gun.ScriptableGun.ClipSize;
      _gunStateChangedGameEvent?.Raise(new GunStateEventData(currentAmmo, _gun.ScriptableGun.ClipSize));
    }
  }
}