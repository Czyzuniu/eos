using UnityEngine;

namespace Runtime.Behaviours.Controller.Weapon.Gun {
  public class GunAudioController : MonoBehaviour {
    private AudioSource _audioSource;
    private Gun _gun;

    private void Awake() {
      _gun = GetComponent<Gun>();
      _audioSource = GetComponent<AudioSource>();
    }

    public void PlayFireSound() {
      _audioSource.clip = _gun.ScriptableGun.FiringSoundEffect;
      _audioSource.Play();
    }
  }
}