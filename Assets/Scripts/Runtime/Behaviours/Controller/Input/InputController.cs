using System;
using Runtime.Behaviours.Controller.Attributes;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.Behaviours.Controller.Input {
  public class InputController : MonoBehaviour {
    [SerializeField]
    private GameEvent _playerDeathEventListener;
    
    [SerializeField]
    private GameObjectGameEvent _playerInitialisedEventListener;
    
    [Header("Character Input Values")]
    public Vector2 move;
    public Vector2 look;

    [Header("Mouse Cursor Settings")]
    public bool cursorLocked = true;
    public bool cursorInputForLook = true;

    public void OnLook(InputValue value) {
      if (cursorInputForLook) {
        LookInput(value.Get<Vector2>());
      }
    }

    private void OnEnable() {
      _playerDeathEventListener.AddListener(UnlockCursor);
      _playerInitialisedEventListener.AddListener(LockCursor);
    }

    public void UnlockCursor() {
      SetCursorState(CursorLockMode.Confined);
    }
    
    public void LockCursor() {
      SetCursorState(CursorLockMode.Locked);
    }
    
    private void OnDisable() {
      _playerDeathEventListener.RemoveListener(UnlockCursor);
      _playerInitialisedEventListener.RemoveListener(LockCursor);
    }

    public void MoveInput(Vector2 newMoveDirection) {
      move = newMoveDirection;
    }

    public void LookInput(Vector2 newLookDirection) {
      look = newLookDirection;
    }

    private void OnApplicationFocus(bool hasFocus) {
      if (GetComponent<PlayerStatistics>().IsDead) {
        UnlockCursor();
      }
      else {
        LockCursor();
      }
    }

    private void SetCursorState(CursorLockMode cursorLockMode) {
      Cursor.lockState = cursorLockMode;
    }

    private void Update() {
      MoveInput(new Vector2(UnityEngine.Input.GetAxis("Horizontal"), UnityEngine.Input.GetAxis("Vertical")));
    }
  }

}