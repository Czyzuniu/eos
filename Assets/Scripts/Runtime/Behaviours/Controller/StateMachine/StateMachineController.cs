using System;
using Runtime.ScriptableObjects.StateMachine;
using UnityEngine;

public class StateMachineController : MonoBehaviour {
   [SerializeField]
   private BaseState _currentState;
   private BaseState _baseLayerState;


   public BaseState BaseLayerState {
     get {
       return _baseLayerState;
     }
     set {
       _baseLayerState = value;
     }
   }

   public BaseState CurrentState {
     get {
       return _currentState;
     }
   }

   private void Update() {
     if (_currentState) {
       _currentState.OnUpdate(gameObject);
     }
   }

   public void EnterState(BaseState newState) {
     _currentState = newState;
     CurrentState.OnEnter(gameObject);
   }
   
   public void ExitState() {
     CurrentState.OnExit(gameObject);
   }

}
