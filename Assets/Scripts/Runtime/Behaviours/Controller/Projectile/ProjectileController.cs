using Photon.Pun;
using Runtime.Models.Event;
using Runtime.ScriptableObjects.Entity;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Projectile {
  [RequireComponent(typeof(CollisionController))]
  public class ProjectileController : MonoBehaviourPun {
    private Rigidbody _rigidbody;
    private Ammo _ammo;

    public Ammo Ammo {
      get {
        return _ammo;
      }
    }

    private void Awake() {
      _rigidbody = GetComponent<Rigidbody>();
    }

    public void Fire(WeaponShootProjectileEventData data, Ammo ammo) {
      _ammo = ammo;
      _rigidbody.AddForce((data.Point - data.SpawnPoint).normalized * 1500f);
      transform.LookAt(data.Point);
    }
    
  }
}
