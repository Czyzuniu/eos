using Runtime.Interfaces;
using Runtime.Models;
using Runtime.ScriptableObjects;
using Runtime.ScriptableObjects.Entity;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Projectile {
  public class CollisionController : MonoBehaviour {
    private bool didHit;
    private ProjectileController _projectileController;

    private void OnCollisionEnter(Collision collision) {
      if (!didHit) {
        _projectileController = GetComponent<ProjectileController>();
        Ammo ammo = _projectileController.Ammo; 
        Destroy(Instantiate(ammo.HitFx, collision.contacts[0].point, Quaternion.FromToRotation(Vector3.up, collision.contacts[0].normal)), 5f);
        Destroy(gameObject);
        collision.transform.root.gameObject.GetComponent<IDamageable>()?.ITakeDamage(new HitData(25, collision.transform.root));
      }
      didHit = true;
    }
  }
}