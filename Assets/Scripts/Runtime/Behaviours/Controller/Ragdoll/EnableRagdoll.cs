using System;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Ragdoll {
  public class EnableRagdoll : MonoBehaviour {
    private Animator _animator;
    private void Awake() {
      _animator = GetComponent<Animator>();
    }

    private void Start() {
      _animator.enabled = false;
    }

  }
}