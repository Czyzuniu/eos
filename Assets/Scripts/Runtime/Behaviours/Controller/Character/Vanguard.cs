using Runtime.Behaviours.Controller.Character.Base;
using Runtime.Interfaces;

namespace Runtime.Behaviours.Controller.Character {
  public class Vanguard : BaseCharacter, IAttack {
    private IGun _weapon;
    
    private void Awake() {
      _weapon = GetComponentInChildren<IGun>();
    }

    public void Fire() {
      _weapon.Fire();
    }

    public void ReloadAnimationEvent() {
      _weapon.Reload();
    }
  }
}