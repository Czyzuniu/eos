using Cinemachine;
using Runtime.Behaviours.Controller.Player;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Cameras {
  public class AimCameraInitializer : MonoBehaviour {
    private CinemachineVirtualCamera _camera;
    [SerializeField]
    private GameObjectGameEvent playerInitialisedEvent;
    void Awake() {
      playerInitialisedEvent.AddListener(OnPlayerInitialised);
      _camera = GetComponent<CinemachineVirtualCamera>();
      Cursor.lockState = CursorLockMode.Locked;
    }
  
    void OnPlayerInitialised(GameObject initialisedPlayer) {
      _camera.m_Follow = initialisedPlayer.GetComponent<PlayerConfig>().CinemachineCameraTarget;
    }
  }
}