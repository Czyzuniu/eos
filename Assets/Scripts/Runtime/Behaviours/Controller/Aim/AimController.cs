using Cinemachine;
using CODE_GENERATION.Events.Game_Events;
using Photon.Pun;
using Runtime.Models;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Aim {
  public class AimController : MonoBehaviourPun {
    Vector3 mouseWorldPosition = Vector3.zero;
    [SerializeField]
    private AimPointDetailsGameEvent _aimPointDetailsGameEvent;
    
    [SerializeField]
    private CinemachineVirtualCamera aimCamera;

    private void Update() {
      Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
      Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
      
      if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f)) {
        mouseWorldPosition = raycastHit.point;
      }

      Vector3 worldAimTarget = mouseWorldPosition;
      worldAimTarget.y = transform.position.y;

      var targetRotation = Quaternion.LookRotation(worldAimTarget - transform.position, Vector3.up);
      transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);

      if (photonView.IsMine) {
        _aimPointDetailsGameEvent?.Raise(new AimPointDetails(mouseWorldPosition, raycastHit.normal, raycastHit.collider.gameObject));
      }
    }
  }
} 