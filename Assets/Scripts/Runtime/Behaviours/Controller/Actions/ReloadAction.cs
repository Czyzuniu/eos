using Photon.Pun;
using Runtime.ScriptableObjects;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Actions {
  public class ReloadAction : MonoBehaviourPun {
    private InputControls _inputControls;
    [SerializeField]
    private ReloadCommand _command;
    
    private void Awake() {
      _inputControls = new InputControls();
      _inputControls.Player.Reload.performed += _ => Reload();
    }

    private void Reload() {
      _command.Execute(gameObject);
    }
    

    private void OnEnable() {
      _inputControls.Enable();
    }

    private void OnDisable() {
      _inputControls.Disable();
    }
  }
}