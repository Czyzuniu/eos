using Photon.Pun;
using Runtime.ScriptableObjects;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Actions {
  public class FireAction : MonoBehaviourPun {
    private InputControls _inputControls;
    
    [SerializeField]
    private Command _command;
    
    private void Awake() {
      _inputControls = new InputControls();
    }

    private void OnFire() {
      _command.Execute(gameObject);
    }

    private void Update() {
      if (_inputControls.Player.Fire.IsPressed()) {
        if (photonView.IsMine) {
          OnFire();
        }
      }
    }

    private void OnEnable() {
      _inputControls.Enable();
    }

    private void OnDisable() {
      _inputControls.Disable();
    }
  }
}