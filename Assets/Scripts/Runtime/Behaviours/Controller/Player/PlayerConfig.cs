using Photon.Pun;
using UnityEngine;

namespace Runtime.Behaviours.Controller.Player {
  public class PlayerConfig : MonoBehaviourPun {
    [SerializeField]
    private GameObject _canvas;
    [SerializeField]
    private GameObject animationAimRigTarget;
    
    [SerializeField]
    private Transform cinemachineCameraTarget;
    public Transform CinemachineCameraTarget {
      get {
        return cinemachineCameraTarget;
      }
    }

    public GameObject AnimationAimRigTarget {
      get {
        return animationAimRigTarget;
      }
    }

    private void Awake() {
      _canvas.SetActive(photonView.IsMine);
    }
  }
}