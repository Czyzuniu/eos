using Photon.Pun;
using Runtime.Serialization;
using UnityEngine;

namespace Runtime.Networking {
    public class NetworkController : MonoBehaviourPunCallbacks
    {
      // Start is called before the first frame update
        void Start() {
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster() {
            Debug.Log("We have connected to the server! " + PhotonNetwork.CloudRegion);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
