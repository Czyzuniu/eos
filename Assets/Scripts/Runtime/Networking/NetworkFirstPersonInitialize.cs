using Photon.Pun;
using UnityEngine;

public class NetworkFirstPersonInitialize : MonoBehaviourPun
{
  private void Awake() {
    if (photonView.IsMine) {
      transform.localScale = Vector3.zero;
    }
  }
}
