using Photon.Pun;
using Runtime.Static;
using UnityEngine;

namespace Runtime.Networking {
    public class LobbyRoomController : MonoBehaviourPunCallbacks {
        public override void OnEnable() {
            PhotonNetwork.AddCallbackTarget(this);
        }

        public override void OnDisable() {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        
        
        public override void OnJoinedRoom() {
            StartGame();
        }

        private void StartGame() {
            if (PhotonNetwork.IsMasterClient) {
                PhotonNetwork.LoadLevel(Levels.Kulfonlandia);
            }
        }
    }
}
