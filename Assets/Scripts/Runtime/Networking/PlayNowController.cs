using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

namespace Runtime.Networking {
    public class PlayNowController : MonoBehaviourPunCallbacks {
        [SerializeField]
        private GameObject playNowButton;
        
        [SerializeField]
        private TMP_InputField playerNameInput;

        [SerializeField]
        private int roomSize;

        public override void OnConnectedToMaster() {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void Play() {
            PhotonNetwork.SendRate = 64;
            JoinOrCreate();
        }


        void JoinOrCreate() {
            RoomOptions roomOptions = new RoomOptions() {IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomSize};
            PhotonNetwork.JoinOrCreateRoom("MainRoom", roomOptions, TypedLobby.Default);
        }

        public override void OnCreateRoomFailed(short returnCode, string message) {
            Debug.LogError("failed to create a room " +  returnCode + " " + message);
        }

    }
}
