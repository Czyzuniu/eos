using Photon.Pun;
using Runtime.Behaviours.Controller.Actions;
using Runtime.Behaviours.Controller.Aim;
using Runtime.Behaviours.Controller.Attributes;
using Runtime.Behaviours.Controller.Hit;
using Runtime.Behaviours.Controller.Input;
using Runtime.Behaviours.Controller.Movement;
using Runtime.Behaviours.Controller.Player;
using Runtime.Utilities;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using StateMachine = Runtime.StateBehaviours.Core.StateMachine;

namespace Runtime.Networking {
  public class NetworkInitialise : MonoBehaviourPunCallbacks {
    void Awake() {
    
      if (photonView.IsMine) {
        gameObject.tag = "Untagged";
      }
      TransformUtils.SetLayerRecursively(gameObject, photonView.IsMine ? LayerMask.NameToLayer("Player") : LayerMask.NameToLayer("PlayerOther"));
      if (!photonView.IsMine) {
        DestroyClientScripts();
      }
    }

    private void DestroyClientScripts() {
      Destroy( GetComponent<MovementController>());
      Destroy( GetComponent<InputController>());
      Destroy( GetComponent<AimController>());
      Destroy( GetComponent<PlayerInput>());
      Destroy( GetComponent<InputController>());
      Destroy( GetComponent<PlayerConfig>());
      Destroy( GetComponent<PlayerHitController>());
      Destroy( GetComponent<StateMachineController>());
      Destroy( GetComponent<ReloadAction>());
      Destroy( GetComponent<FireAction>());
      foreach (StateMachine stateMachine in GetComponent<Animator>().GetBehaviours<StateMachine>()) {
        Destroy(stateMachine);
      }
      Destroy(this);
    }
  }


}