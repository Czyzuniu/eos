﻿using Runtime.ScriptableObjects.StateMachine;
using UnityEngine;
using UnityEngine.Animations;

namespace Runtime.StateBehaviours.Core {
  public class StateMachine : StateMachineBehaviour {
    private StateMachineController _stateMachineController;
    [SerializeField]
    private BaseState _state;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
      if (!_stateMachineController) {
        _stateMachineController = animator.GetComponent<StateMachineController>();
      }
      _stateMachineController.EnterState(_state);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
      if (layerIndex == 0) {
        _stateMachineController.BaseLayerState = _state;
      }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller) {
      _stateMachineController.ExitState();
      if (layerIndex > 0) {
        _stateMachineController.EnterState(_stateMachineController.BaseLayerState);
      }
    }
  }
}
