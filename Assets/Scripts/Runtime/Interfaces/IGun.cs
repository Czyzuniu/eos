using Runtime.ScriptableObjects.Entity;

namespace Runtime.Interfaces {
  public interface IMuzzle {
    public void Flash();
  }

  public interface IBarrel {
    public void Fire();
  }

  public interface IGunState {
    public int GetCurrentAmmo();
    public void UseAmmo();
    public void Reload();

    public bool CanFire();
  }
}