namespace Runtime.Interfaces {
  public interface IWeapon {
    public void Fire();
  }

  public interface IGun : IWeapon {
    public void Reload();
  }
}