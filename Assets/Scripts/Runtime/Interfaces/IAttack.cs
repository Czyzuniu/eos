namespace Runtime.Interfaces {
  public interface IAttack {
    public void Fire();
  }
}