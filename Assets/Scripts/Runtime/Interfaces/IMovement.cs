using UnityEngine;

namespace Runtime.Interfaces
{
    public interface IMovement
    {
        Vector2 GetMovementVector();
    }
}