using Runtime.Models;

namespace Runtime.Interfaces {
  public interface IDamageable {
    public void ITakeDamage(HitData hitData);
  }
}