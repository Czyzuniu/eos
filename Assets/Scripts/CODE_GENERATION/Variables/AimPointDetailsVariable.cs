using Runtime.Models;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace CODE_GENERATION.Variables
{
	[System.Serializable]
	public class AimPointDetailsEvent : UnityEvent<AimPointDetails> { }

	[CreateAssetMenu(
	    fileName = "AimPointDetailsVariable.asset",
	    menuName = SOArchitecture_Utility.VARIABLE_SUBMENU + "AimPointDetails",
	    order = 120)]
	public class AimPointDetailsVariable : BaseVariable<AimPointDetails, AimPointDetailsEvent>
	{
	}
}