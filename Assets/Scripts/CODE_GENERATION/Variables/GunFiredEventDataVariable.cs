using Runtime.Models.GameEvent;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectArchitecture
{
	[System.Serializable]
	public class GunFiredEventDataEvent : UnityEvent<GunStateEventData> { }

	[CreateAssetMenu(
	    fileName = "GunFiredEventDataVariable.asset",
	    menuName = SOArchitecture_Utility.VARIABLE_SUBMENU + "GunFiredEventData",
	    order = 120)]
	public class GunFiredEventDataVariable : BaseVariable<GunStateEventData, GunFiredEventDataEvent>
	{
	}
}