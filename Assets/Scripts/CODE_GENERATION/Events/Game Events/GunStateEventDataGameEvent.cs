using Runtime.Models.GameEvent;
using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[System.Serializable]
	[CreateAssetMenu(
	    fileName = "GunFiredEventDataGameEvent.asset",
	    menuName = SOArchitecture_Utility.GAME_EVENT + "GunFiredEventData",
	    order = 120)]
	public sealed class GunStateEventDataGameEvent : GameEventBase<GunStateEventData>
	{
	}
}