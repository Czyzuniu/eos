using Runtime.Models;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace CODE_GENERATION.Events.Game_Events
{
	[System.Serializable]
	[CreateAssetMenu(
	    fileName = "AimPointDetailsGameEvent.asset",
	    menuName = SOArchitecture_Utility.GAME_EVENT + "AimPointDetails",
	    order = 120)]
	public sealed class AimPointDetailsGameEvent : GameEventBase<AimPointDetails>
	{
	}
}