using System;
using Runtime.Models;
using UnityEngine.Events;

namespace CODE_GENERATION.Events.Responses
{
	[Serializable]
	public sealed class AimPointDetailsUnityEvent : UnityEvent<AimPointDetails>
	{
	}
}