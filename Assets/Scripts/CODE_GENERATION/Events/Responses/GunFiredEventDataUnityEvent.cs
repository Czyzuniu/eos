using Runtime.Models.GameEvent;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectArchitecture
{
	[System.Serializable]
	public sealed class GunFiredEventDataUnityEvent : UnityEvent<GunStateEventData>
	{
	}
}