using CODE_GENERATION.Events.Game_Events;
using CODE_GENERATION.Events.Responses;
using Runtime.Models;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace CODE_GENERATION.Events.Listeners
{
	[AddComponentMenu(SOArchitecture_Utility.EVENT_LISTENER_SUBMENU + "AimPointDetails")]
	public sealed class AimPointDetailsGameEventListener : BaseGameEventListener<AimPointDetails, AimPointDetailsGameEvent, AimPointDetailsUnityEvent>
	{
	}
}