using Runtime.Models.GameEvent;
using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[AddComponentMenu(SOArchitecture_Utility.EVENT_LISTENER_SUBMENU + "GunFiredEventData")]
	public sealed class GunFiredEventDataGameEventListener : BaseGameEventListener<GunStateEventData, GunStateEventDataGameEvent, GunFiredEventDataUnityEvent>
	{
	}
}