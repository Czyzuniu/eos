using CODE_GENERATION.Variables;
using Runtime.Models;
using ScriptableObjectArchitecture;

namespace CODE_GENERATION.References
{
	[System.Serializable]
	public sealed class AimPointDetailsReference : BaseReference<AimPointDetails, AimPointDetailsVariable>
	{
	    public AimPointDetailsReference() : base() { }
	    public AimPointDetailsReference(AimPointDetails value) : base(value) { }
	}
}