using Runtime.Models.GameEvent;
using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[System.Serializable]
	public sealed class GunFiredEventDataReference : BaseReference<GunStateEventData, GunFiredEventDataVariable>
	{
	    public GunFiredEventDataReference() : base() { }
	    public GunFiredEventDataReference(GunStateEventData value) : base(value) { }
	}
}